﻿using UnityEngine;
using System.Collections;

public class BumblebeeControl : MonoBehaviour {

	public GameObject Wings;
	public GameObject BumblebeeSprite;
	//private bool up = false;
	//private bool right = false;
	//private bool left = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log ("Vy = " + this.rigidbody2D.velocity.y);

	}

	void FixedUpdate() 
	{
		bool up = false;
		bool right = false;
		bool left = false;

		Rect screenLeft = new Rect(0f, 0f, Screen.width / 2, Screen.height);
		Rect screenRight = new Rect(Screen.width / 2, 0f, Screen.width / 2, Screen.height);
		
		//if (Input.touchCount > 0)
		for (var i = 0; i < Input.touchCount; ++i)
		{
			var touchPos = Input.GetTouch(i).position;
			print (touchPos);
			if (screenLeft.Contains(touchPos))
			{
				left = true;
				Debug.Log("screenLeft touched");
			}
			if (screenRight.Contains(touchPos))
			{
				right = true;
				Debug.Log("screenRight touched");
			}  
		}
		/*
		for (var i = 0; i < Input.touchCount; ++i) {
			if (Input.GetTouch(i).phase == TouchPhase.Began) {
				Input.GetTouch(i).position;
			}
		}
		*/
		
		if (Input.GetKey (KeyCode.UpArrow)) up = true;
		//if (Input.GetKeyUp (KeyCode.UpArrow)) up = false;
		
		if (Input.GetKey (KeyCode.RightArrow)) right = true;
		//if (Input.GetKeyUp (KeyCode.RightArrow)) right = false;
		
		if (Input.GetKey (KeyCode.LeftArrow)) left = true;
		
		Vector2 force = new Vector2(0f, 0f);
		if (right && left) force = new Vector2 (0f, 30f);
		else if (up) force = new Vector2 (0f, 30f);
		else if (right) force = new Vector2 (20f, 30f);
		else if (left) force = new Vector2 (-20f, 30f);

		if (right || left || up) 
		{
			TouchSense.instance.playBuiltinEffect(111);
		}
		else 
		{
			TouchSense.instance.stopPlayingBuiltinEffect();
		}
		
		if (force.x < 0) {
			BumblebeeSprite.transform.localPosition = new Vector2(0.091f, 0.166f);
			BumblebeeSprite.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
		}
		else if (force.x > 0) {
			BumblebeeSprite.transform.localPosition = new Vector2(-0.091f, 0.166f);
			BumblebeeSprite.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
		}

		//Debug.Log ("Force = " + force);
		
		Wings.rigidbody2D.AddForceAtPosition (force, new Vector2 (0f, 0f));
	}
}
